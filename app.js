// daily accordion

let accordionItems = document.querySelector(".accordion-items_1");
let arrowDown = document.querySelector(".arrow-down_1");
let arrowUp = document.querySelector(".arrow-up_1");

arrowDown.addEventListener("click", () => {
  arrowDown.style.display = "none";
  arrowUp.style.display = "block";
  accordionItems.style.display = "block";
});

arrowUp.addEventListener("click", () => {
  arrowDown.style.display = "block";
  arrowUp.style.display = "none";
  accordionItems.style.display = "none";
});

// Sports accordion

let sportsAccordionItems = document.querySelector(".accordion-items_2");
let sportsArrowDown = document.querySelector(".arrow-down_2");
let sportsArrowUp = document.querySelector(".arrow-up_2");

sportsArrowDown.addEventListener("click", () => {
  sportsArrowDown.style.display = "none";
  sportsArrowUp.style.display = "block";
  sportsAccordionItems.style.display = "block";
});

sportsArrowUp.addEventListener("click", () => {
  sportsArrowDown.style.display = "block";
  sportsArrowUp.style.display = "none";
  sportsAccordionItems.style.display = "none";
});
